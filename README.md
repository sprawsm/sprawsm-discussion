Superawesome Design Discussions
===============================

[Superawesome][1] came up with a simple way to utilize a free comment platform [Disqus][2] in order to provide a simple way to discuss mockups with your colleagues and customers.

[See the demo and usage instructions.][3]

Terms of Use
==============

This code is provided as-is, without any warranty or guarantees. Sprawsm doo can not be held responsible for any damage or potential breach of privacy you may face by using this code. Sprawsm doo is not affiliated with Disqus in any way, we just like their awesome product. 

[1]: http://sprawsm.com/
[2]: http://disqus.com/
[3]: http://sprawsm.com/discussions/
